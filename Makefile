# PROJECT VARIABLES
PROJECT_FOLDER=./
PROJECT_NAME=#"TODO"
PROJECT_COMPANY=#"TODO"
SCHEME_NAME=#"TODO"
SCRIPT_PATH=./Scripts
EXPORT_PATH=./Export
DEVELOP_BRANCH=develop
STAGING_BRANCH=staging
MASTER_BRANCH=master

## 1 if Hockey testers should be notified. 0 otherwise.
NOTIFY=1

## Do not include the project name for Bundle ID Prefix.
## If your bundle ID is com.prolificinteractive.Kourt, then APPLEDOC_BUNDLE_ID_PREFIX should be 'com.prolificinteractive'
APPLEDOC_BUNDLE_ID_PREFIX=#"TODO"


# STAGING VARIABLES
CONFIG_NAME_STAGING=#"TODO"
HOCKEY_TOKEN_STAGING=#"TODO"
HOCKEY_APP_ID_STAGING=#"TODO"
## NOTE: The following path variables should be defined without quotes.
EXPORT_OPTIONS_PLIST_PATH_STAGING=#TODO ### ./OPEN\ Forum/StagingExportOptions.plist ###
INFO_PLIST_PATH_STAGING=#TODO ### ./OPEN\ Forum/Supporting\ Files/Info.plist ###
SETTINGS_PLIST_PATH_STAGING=#TODO ### OPTIONAL ### ./OPEN\ Forum/Supporting\ Files/Settings/Settings.bundle/Root.plist ###
TAG_PREFIX_STAGING=staging


# RELEASE VARIABLES
CONFIG_NAME_RELEASE=#"TODO"
EXPORT_OPTIONS_PLIST_PATH_RELEASE=#TODO
INFO_PLIST_PATH_RELEASE=#TODO
SETTINGS_PLIST_PATH_RELEASE=#TODO
TAG_PREFIX_RELEASE=release
ITUNES_CONNECT_USER=#"TODO"
KEYCHAIN_LABEL=#"TODO"
## App ID. You can find this in iTunes Connect under App Information section (listed as Apple ID).
APP_ID=#"TODO"



# SETUP
install: install-space-commander install-documentation-tool

install-space-commander:
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/install_space_commander.sh

install-documentation-tool:
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/install_documentation.sh

create-export-directory:
	cd ${PROJECT_FOLDER}
	mkdir -p ${EXPORT_PATH}


# STAGING TARGETS
build-staging: create-export-directory
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/create_build.sh ${PROJECT_NAME} ${SCHEME_NAME} ${CONFIG_NAME_STAGING} ${EXPORT_OPTIONS_PLIST_PATH_STAGING} ${EXPORT_PATH}

distribute-staging: build-staging
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/distribute_build_hockey.sh ${HOCKEY_TOKEN_STAGING} ${HOCKEY_APP_ID_STAGING} \
		 ${EXPORT_PATH}/${SCHEME_NAME}.ipa \
		 ${EXPORT_PATH}/${SCHEME_NAME}.app.dSYM.zip \
		 "`${SCRIPT_PATH}/generate_changelog.sh staging`" \
		 ${NOTIFY}

tag-staging:
	${SCRIPT_PATH}/tag_build.sh ${TAG_PREFIX_STAGING} ${INFO_PLIST_PATH_STAGING}

bump-staging: tag-staging
	# Switch to develop
	git checkout ${DEVELOP_BRANCH}
	git merge origin/${DEVELOP_BRANCH}

	# Then bump build number
	${SCRIPT_PATH}/bump_version.sh ${INFO_PLIST_PATH_STAGING} ${SETTINGS_PLIST_PATH_STAGING}
	git commit -am "Bumped build version."
	git push origin ${DEVELOP_BRANCH}

generate-changelog-staging:
	${SCRIPT_PATH}/generate_changelog.sh staging


# RELEASE TARGETS
build-release: create-export-directory
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/create_build.sh ${PROJECT_NAME} ${SCHEME_NAME} ${CONFIG_NAME_RELEASE} ${EXPORT_OPTIONS_PLIST_PATH_RELEASE} ${EXPORT_PATH}

distribute-release: build-release
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/distribute_build_itunesconnect.sh ${EXPORT_PATH}/${SCHEME_NAME}.ipa \
		${ITUNES_CONNECT_USER} \
		${KEYCHAIN_LABEL} \
		${APP_ID}

tag-release:
	${SCRIPT_PATH}/tag_build.sh ${TAG_PREFIX_RELEASE} ${INFO_PLIST_PATH_RELEASE}


# DOCUMENTATION
documentation:
	# appledoc command reports an error code of 1 for some reason (even if the documentation was installed properly)
	# in order to get around this, we dump the error code into /dev/null so that way the documentation command can finish properly
	appledoc --project-name ${PROJECT_NAME} --project-company ${PROJECT_COMPANY} --ignore \
		Pods --ignore Frameworks --company-id ${APPLEDOC_BUNDLE_ID_PREFIX} \
	 	--output ./Documentation ./ 2> /dev/null; true
	rm -rf ./Documentation


# CODE FORMATTING
clean-code-staged:
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/spacecommander/format-objc-files.sh

clean-code-all:
	cd ${PROJECT_FOLDER}
	${SCRIPT_PATH}/spacecommander/format-objc-files-in-repo.sh

set-keys:
	${SCRIPT_PATH}/set_keys.sh ./Keys.plist

