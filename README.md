# `TODO`

## Purpose
`TODO`

## Authors

### iOS
`TODO`

### Project Management
`TODO`

## iOS Specificities

#### Dev Notes
`TODO`

#### Cocoapods
This project relies on [cocoapods](cocapods.org) to manage its dependencies

#### appledoc
* This project uses appledoc to extract documentation from Objective-C code.
* [GitHub Repo](https://github.com/tomaz/appledoc)

#### Install/Build Notes
* Clone repo
* install pods (`pod install`)
* If Crashlytics isn't installed on your computer yet:
  * Go to crashlytics.com, 
  * Install Xcode plugin, 
  * Select 'app already setup' when asked, 
  * Compile project, you are all set!
  