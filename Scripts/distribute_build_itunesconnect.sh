#!/bin/sh

# This script submits a given IPA file to iTunes Connect. 
# This requires the device that is making the build to have the iTunesConnect password stored in the keychain.
#
# $1: IPA file to be submitted to iTunes Connect.
# $2: Email address of the iTunes Connect account used to create the app.
# $3: Name of the keychain item which contains the password for the iTunes Connect username.
# $4: App ID. You can find this in iTunes Connect under App Information section (listed as Apple ID).

IPA_FILE=$1
ITUNES_CONNECT_USER=$2
KEYCHAIN_LABEL=$3
APP_ID=$4

KEYCHAIN_PASSWORD=$(security find-generic-password -l "${KEYCHAIN_LABEL}" -w)

# Check to make sure there is a password by that name
if [ "$KEYCHAIN_PASSWORD" == "" ]
then
  echo "\nNo password found in keychain named ${KEYCHAIN_LABEL}. Build aborted.\n"
  exit 2
fi

echo '\nUploading to iTunes Connect'
./Scripts/upload-to-itunes-connect.sh ${IPA_FILE} ${ITUNES_CONNECT_USER} ${KEYCHAIN_PASSWORD} ${APP_ID}



