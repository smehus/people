#!/bin/sh

# $1: Prefix for the tag. (e.g. staging or dev)
# $2: Path to info plist file.

TAG_PREFIX=$1
PLIST=$2

CFBundleVersion=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "${PLIST}")
CFBundleShortVersionString=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "${PLIST}")

TAG=${TAG_PREFIX}-${CFBundleShortVersionString}-${CFBundleVersion}

# Create a tag with a given prefix and build version.
git tag ${TAG}

# Push the tag to origin.
git push origin ${TAG}
