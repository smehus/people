#!/bin/sh

# $1: Hockey API token used for authentication.
# $2: Hockey App ID of the IPA file to be uploaded.
# $3: IPA file to be uploaded to Hockey.
# $4: dSYM file to be uploaded to Hockey.
# $5: Release notes for the current build.
# $6: Flag indicating whether testers should be notified of the current build.

APP_TOKEN=$1
APP_ID=$2
IPA_FILE=$3
DSYM_FILE=$4
NOTES=$5
NOTIFY=$6
POST_URL="https://rink.hockeyapp.net/api/2/apps/$APP_ID/app_versions/upload"

echo "\nUploading IPA file to Hockey"

curl \
  -F "ipa=@$IPA_FILE" \
  -F "dsym=@$DSYM_FILE" \
  -F "notes=$NOTES" \
  -F "notes_type=0" \
  -F "notify=$NOTIFY" \
  -F "status=2" \
  -H "X-HockeyAppToken:$APP_TOKEN" \
  $POST_URL || exit 1

echo "\n\nHockey upload complete."


