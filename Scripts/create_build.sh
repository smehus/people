#!/bin/sh

# $1: Name of the project to build.
# $2: Name of the scheme to build.
# $3: Build configuration associated with the scheme ($2).
# $4: Plist file where export options are specified.
# $5: Path to the directory where resulting IPA and build files should be written.

PROJECT_NAME=$1
SCHEME_NAME=$2
CONFIG_NAME=$3
EXPORT_PLIST=$4
EXPORT_PATH=$5
CURRENT_DATE=$(date '+%Y-%m-%d')

check_staged_changes() {
  # Checks to see if there are any change in the local diff. If there are, that means things are different between
  # the HEAD branch being built and our local store. If this is so, the build will abort.
  #
  # wc -c creates a bunch of leading white space; tr -d command removes it and just gives the raw number for checking.
  has_changes=$(git diff HEAD | wc -c | tr -d '[[:space:]]')

  if [ "$has_changes" != "0" ]
  then
  	echo "\nThe project has unstaged changes. Aborting build. Please commit them and try again.\n"
  	exit 1
  fi
}

uses_cocoapods() {
  num_podfiles=$(ls | grep Podfile -c)
  
  if [ $num_podfiles -gt 0 ]; 
  then
    return 0
  else
    return 1
  fi
}

create_archive() {
  if uses_cocoapods; 
  then
    echo "\nSanitizing CocoaPods"
    rm -rf ./Pods
    pod install

    echo "\nArchiving project"
    xcodebuild -workspace $PROJECT_NAME".xcworkspace" -scheme $SCHEME_NAME archive > /dev/null || exit 1
  else
    xcodebuild -project $PROJECT_NAME".xcodeproj" -scheme $SCHEME_NAME archive > /dev/null || exit 1
  fi
}

create_ipa() {  
  archive_dir=~/Library/Developer/Xcode/Archives/$CURRENT_DATE
  archive_name=$(ls -tr "$archive_dir" | tail -1)
  archive_path=$archive_dir/$archive_name
  dsym_path=$archive_path/dSYMs
  
  echo "\nCreating IPA file"
  xcodebuild -exportArchive -archivePath "$archive_path" -exportPath $EXPORT_PATH -configuration $CONFIG_NAME -exportOptionsPlist $EXPORT_PLIST || exit 1

  echo "\nZipping symbol file"
  cd "$dsym_path"
  dsym_name=$(ls | grep app.dSYM | tail -1)
  zip_name="${SCHEME_NAME}.app.dSYM.zip"
  zip -vrTy $zip_name $dsym_name || exit 1

  cd -
  zip_file=$dsym_path/$zip_name
  mv "$zip_file" "$EXPORT_PATH"
}

check_staged_changes
create_archive
create_ipa

echo "Exported IPA and dSYM files at \`$EXPORT_PATH\`."
 