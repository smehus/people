#!/bin/sh

if [ -d "./Scripts/spacecommander/" ]; then
  echo 'Space commander is already set up.'
else
  cd ./Scripts

  echo 'Downloading Space Commander'
  git clone git@github.com:square/spacecommander.git

  cd spacecommander
  git checkout tags/1.0.0 > /dev/null 2>&1

  # Replace the hook with our custom version
  echo 'Replacing the pre-commit hook'
  rm format-objc-hook
  curl -0 https://gist.githubusercontent.com/prolificmobile/32e1e713fddfbab99957/raw/00f2fcb002004103d1b8711143cb795d35844300/format-objc-hook >> format-objc-hook
  chmod 755 format-objc-hook

  cd ../../

  if [ -z $(grep "spacecommander" "./.gitignore") ];
  then
    echo "\n\n# Space Commander" >> ./.gitignore
    echo "Scripts/spacecommander/" >> ./.gitignore
  fi

  echo 'Setting up Space Commander'
  ./Scripts/spacecommander/setup-repo.sh
fi