#!/bin/sh

key_html_tag="<key>"
key_html_tag_end="</key>"
value_html_tag="<string>"
value_html_tag_end="</string>"

filename=$1

if [ ! -f "$filename" ]
    then
    echo "$filename not found.  Make sure to add a file named \"$filename\" with the proper keys to your project's folder."
    exit 1
fi

while IFS='' read -r line || [[ -n "$line" ]]; do
    if [[ $line =~ "$key_html_tag" ]]; then
        key=`echo "$line" | sed -e 's/<\/b>/-/g' -e 's/<[^>]*>//g' | sed 's/[[:space:]]//g'`
    fi

    if [[ $line =~ "$value_html_tag" ]]; then
        value=`echo "$line" | sed -e 's/<\/b>/-/g' -e 's/<[^>]*>//g' | sed 's/[[:space:]]//g'`
    fi

    if [ ! -z "$key" ] && [ ! -z "$value" ]; then
        echo "Key: $key - Value: $value"
        pod keys set $key $value
        value=""
        key=""
    fi
done < $filename