#!/bin/sh

# Usage
# 
# The script takes in four arguments as locations of the different plist files.
# Argument 2, 3 and 4 are optional.
# The first argument is used as the path to the info plist file.
# The second argument is used as the path to settings bundle plist.
# WatchKit extension and app plists can also be passed in as the third and fourth arguments respectively.
# 
# The script will read the version number from the build plist and update the files accordingly.


# Auto Increment Version Script
if [ $# -eq 1 ] || [ $# -eq 2 ] || [ $# -eq 4 ]
then
	buildPlist=$1
	CFBundleVersion=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" "$buildPlist")
	CFBundleShortVersionString=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" "$buildPlist")
	CFBundleVersionNew=$(($CFBundleVersion + 1))
	/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $CFBundleVersionNew" "$buildPlist"
	
	# If the second arugment exists, use it as the Settings bundle plist
	if ! [ -z "$2" ]
		then
			settingsPlist=$2
	        fullVersionString=$CFBundleShortVersionString" ("$CFBundleVersionNew")"
        
	        # Find out the number of entries in the bundle
	        preferenceCount=$(/usr/libexec/PlistBuddy -c "Print :PreferenceSpecifiers" "$settingsPlist" | grep -wo "Dict" | wc -l)
	        for ((i=0; i<$preferenceCount; i++))
	        do
	            {
	                # Look for the row that has the "app_version" key
	                Key=$(/usr/libexec/PlistBuddy -c "Print :PreferenceSpecifiers:$i:Key" "$settingsPlist")
	                if [ "$Key" == "app_version" ] 
	                    then
	                        # Update the build number
	                        /usr/libexec/PlistBuddy -c "Set :PreferenceSpecifiers:$i:DefaultValue $fullVersionString" "$settingsPlist"
	                        break
	                fi
	            } &> /dev/null # Suppress the messages for entries where there are no Keys
	        done        
	fi
	
	# If the third arugment exists, use it as the WatchKit extension info plist
	if ! [ -z "$3" ]
	then
		watchKitExtensionPlist=$3
		/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $CFBundleVersionNew" "$watchKitExtensionPlist"
	fi
	
	# If the fourth arugment exists, use it as the WatchKit app info plist
	if ! [ -z "$4" ]
	then
		watchKitAppPlist=$4
		/usr/libexec/PlistBuddy -c "Set :CFBundleVersion $CFBundleVersionNew" "$watchKitAppPlist"
	fi
else 
	echo Usage: info.plist [settings.plist] [watchkit-extension.plist watchkit-app.plist]
fi 