#!/bin/sh

PREFIX=$1
TAG=$(git tag | grep $PREFIX | tail -1)

if [ "$TAG" ]; 
then
   git log --pretty=format:%s $TAG..HEAD | sed '/Merge.*into/d' | sed '/Merged in/d' | sed '/Bumped build version/d'
fi