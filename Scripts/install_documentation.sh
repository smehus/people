#!/bin/sh

# We are using this script because installation using homebrew does not seem to work as expected.

echo 'Downloading Appledocs'
git clone git@github.com:tomaz/appledoc.git 
cd ./appledoc
echo 'Installing'
sudo sh install-appledoc.sh > /dev/null
cd ..
sudo rm -rf ./appledoc > /dev/null
echo 'Appledoc installation completed successfully.'